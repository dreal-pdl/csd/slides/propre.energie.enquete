# Le registre des points d'injection de biométhane, ODRE  {#biomethane}

```{r biomethane}

param_page = "biomethane"

rep_source <- reponses %>% 
  select(id, REG, NOM_REG, contains(param_page), pond_reg, investi, EC) %>% 
  mutate(EC = EC > 0) %>% 
  # on ajoute un jeu de ligne pour équilibrer les représentations régionales (un seul car la 3e réponse de AURA est à NA)
  bind_rows(filter(., pond_reg == 1)) %>% 
  pivot_longer(names_to = "sous_quest", values_to = "avis_pertinence", cols = contains("1_SQ0") ) %>% 
  filter(!is.na(avis_pertinence)) %>% 
  left_join(dico_var, by = c("sous_quest" = "nom")) %>%
  # on ajoute les régions contributrices pour qu'elles prennent part à la catégorie 'toutes regions'
   bind_rows(
    filter(., investi == "régions contributrices") %>% mutate(investi = "toutes régions")
    ) %>% 
  separate(col = lib, into = c("lib", NA), sep = "] ") %>% 
  mutate(sous_quest = gsub(paste0(param_page, "1_"), "", sous_quest),
         lib = gsub("\\[", "", lib))

```

## Descriptif de la source
```{r desc biomethane}
descriptif <- filter(dico_var, nom == paste0(param_page, "0")) %>% 
  pull(lib) %>% 
  gsub(".Dernier millésime ", ". <br>Dernier millésime ", ., fixed = TRUE) %>% 
  gsub("Périodicité : ", "<br>Périodicité : ", ., fixed = TRUE) %>% 
  gsub(".", ". ", ., fixed = TRUE)
  

```

`r descriptif`


## Rappel de l'intérêt suscité par la source

* Position pour toutes les régions : `r filter(rep_sce_top3, source == param_page, investi == "toutes régions") %>% pull(rang)` / 7.   
* Position pour les seules régions contributrices : `r filter(rep_sce_top3, source == param_page, investi == "régions contributrices") %>% pull(rang)` / 7.  

```{r engouement biomethane}
g_rappel <- pertinence_sces %>% 
  filter(nom_court == param_page) %>% 
  ggplot() +
  geom_bar_interactive(aes(x = investi, tooltip = avis_pertinence, fill = avis_pertinence), position = "fill") +
  scale_fill_gouv_discrete(palette = "pal_gouv_qual2") +
  scale_y_continuous(breaks = c(0, 0.25, 0.5, 0.75, 1), labels = c(0, 0.25, 0.5, 0.75, 1) * 100) +
  labs(y = "%", x = "", caption = caption_enq, title = "Répartion des avis sur la pertinence de la source registre biométhane") +
  guides(fill = guide_legend(title = NULL, direction = "horizontal", nrow =  1)) +
  theme( legend.position = 'top') + coord_flip()

girafe(print(g_rappel), width_svg = 12, height_svg = 5, pointsize=13)

```



## Rappel des choix proposés
```{r biomethane choix}
select(rep_source, sous_quest, lib) %>% 
  distinct() %>% 
  bind_cols(nom_court = c("Rmd au dépt", "Rmd maille fine", "Shiny")) %>% 
  kable

```
## Les avis selon le niveau d'engagement de la région

```{r biomethane raph}
g_source <- ggplot(rep_source) +
  geom_bar_interactive(aes(x = sous_quest, tooltip = avis_pertinence, fill = avis_pertinence), position = "fill") +
  scale_fill_gouv_discrete(palette = "pal_gouv_qual2") +
  scale_y_continuous(breaks = c(0, 0.25, 0.5, 0.75, 1), labels = c(0, 0.25, 0.5, 0.75, 1) * 100) +
  scale_x_discrete(breaks = c("SQ001", "SQ002", "SQ003"), labels = c("Rmd au dépt", "Rmd maille fine", "Shiny")) +
  labs(y = "%", x = "", caption = caption_enq, title = "Répartion des avis de pertinence pour la source biométhane") +
  guides(fill = guide_legend(title = NULL, direction = "horizontal", nrow =  1)) +
  facet_grid(. ~ investi) + theme( legend.position = 'top')

girafe(print(g_source), width_svg = 12, height_svg = 7, pointsize=13)

```

## L'avis des services métier
```{r biomethane metier, out.width = '70%'}


g_metier <- filter(rep_source, investi == "toutes régions", EC) %>% 
  distinct() %>% 
  ggplot() +
  geom_bar_interactive(aes(x = sous_quest, tooltip = avis_pertinence, fill = avis_pertinence), position = "fill") +
  scale_fill_gouv_discrete(palette = "pal_gouv_qual2") +
  scale_y_continuous(breaks = c(0, 0.25, 0.5, 0.75, 1), labels = c(0, 0.25, 0.5, 0.75, 1) * 100) +
  scale_x_discrete(breaks = c("SQ001", "SQ002", "SQ003"), labels = c("Rmd au dépt", "Rmd maille fine", "Shiny")) +
  labs(y = "%", x = "", caption = caption_enq, title = "Répartion des avis de pertinence pour la source biométhane des services métiers") +
  guides(fill = guide_legend(title = NULL, direction = "horizontal", nrow =  1)) +
  theme( legend.position = 'top')

girafe(print(g_metier), width_svg = 9, height_svg = 7, pointsize=13)

```


## Les commentaires recueillis

```{r biomethane comm}

filter(rep_source, !is.na(biomethane2)) %>% 
  select(Commentaires = biomethane2) %>% 
  unique %>% 
  kable()

```

## Ce que l'on peut en dire
L'exploitation de cette source arrive en avant dernière position dans le classement des 7 sources proposées.  
L'absence de données sur les DROM n'avait pas été identifiée au lancement de l'enquête et justifie qu'on n'y accorde pas plus d'intérêt à ce stade.
`r emo::ji("smile")`

