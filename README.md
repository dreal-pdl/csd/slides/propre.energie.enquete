Repos de publication des résultats de l'enquête inter-DREAL visant à avancer dans la définition collective du produit à élaborer, dans le cadre d’un 2e projet de publication statistique coordonnée des DREAL selon la méthode ProPRe (Processus de publication reproductible). 

Lien vers le html de présentation des résultats :
https://rdes_dreal.gitlab.io/propre.energie.enquete/index.html
